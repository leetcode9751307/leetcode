def display(root):
    lines, *_ = _display_aux(root)
    for line in lines:
        print(line)


def _display_aux(self):
    # No child.
    if self.right is None and self.left is None:
        line = '%s' % self.val
        width = len(line)
        height = 1
        middle = width // 2
        return [line], width, height, middle

   # Only left child.
    if self.right is None:
        lines, n, p, x = _display_aux(self.left)
        s = '%s' % self.val
        u = len(s)
        first_line = (x + 1) * ' ' + (n - x - 1) * '_' + s
        second_line = x * ' ' + '/' + (n - x - 1 + u) * ' '
        shifted_lines = [line + u * ' ' for line in lines]
        return [first_line, second_line] + shifted_lines, n + u, p + 2, n + u // 2
    # Only right child.
    if self.left is None:
        lines, n, p, x = _display_aux(self.right)
        s = '%s' % self.val
        u = len(s)
        first_line = s + x * '_' + (n - x) * ' '
        second_line = (u + x) * ' ' + '\\' + (n - x - 1) * ' '
        shifted_lines = [u * ' ' + line for line in lines]
        return [first_line, second_line] + shifted_lines, n + u, p + 2, u // 2

    # Two children.
    left, n, p, x = _display_aux(self.left)
    right, m, q, y = _display_aux(self.right)
    s = '%s' % self.val
    u = len(s)
    first_line = (x + 1) * ' ' + (n - x - 1) * \
        '_' + s + y * '_' + (m - y) * ' '
    second_line = x * ' ' + '/' + \
        (n - x - 1 + u + y) * ' ' + '\\' + (m - y - 1) * ' '
    if p < q:
        left += [n * ' '] * (q - p)
    elif q < p:
        right += [m * ' '] * (p - q)
    zipped_lines = zip(left, right)
    lines = [first_line, second_line] + \
        [a + u * ' ' + b for a, b in zipped_lines]
    return lines, n + m + u, max(p, q) + 2, n + u // 2
# Write a recursive algorithm that replaces each single binary tree node with
# multiple nodes with smaller values. Your algorithm is called with two
# parameters: a reference to a TreeNode pointer representing the root of a binary
# tree, and an integer "stretching factor" K. Your function should replace each
# node N with K nodes, each of which stores a data value that is 1/K of N's
# original value, using integer division.

# The new clones of node N should extend from their parent in the same direction
# that N extends from its parent. For example, if N is its parent's left child,
# the stretched clones of N should also be their parent's left child, and vice
# versa if N was a right child. The root node is a special case because it has no
# parent; we will handle this by saying that its stretched clones should extend to
# the left.

# Constraints: Do not modify the signature of the stretch interface

# Signature: `void stretch(root *TreeNode, stretchAmount int)`

# You must use recursion in your solution to visit each tree node

# Diagram: https://i.imgur.com/UgvoVwF.png

# (Mirror: https://i.postimg.cc/gjWBrcrb/diagram.png)


class TreeNode:
    def __init__(self, val):
        self.val = val
        self.left = None
        self.right = None

    def __repr__(self):
        return f"Node({self.val})"


def stretch(root: TreeNode, k: int):

    def helper(node, is_right):
        if not node:
            return
        helper(node.left, False)
        helper(node.right, True)

        node.val = node.val // k

        tmp1 = node.right
        tmp2 = node.left
        node.right = None
        node.left = None
        curr = node
        for i in range(k-1):
            new_node = TreeNode(node.val)
            print(new_node)
            if is_right:
                curr.right = new_node
                curr = curr.right
            else:
                curr.left = new_node
                curr = curr.left
        curr.right = tmp1
        curr.left = tmp2

    helper(root, False)


n12 = TreeNode(12)
n81 = TreeNode(81)
n56 = TreeNode(56)
n34 = TreeNode(34)
n19 = TreeNode(19)
n6 = TreeNode(6)

n12.left = n81
n12.right = n34
n81.right = n56
n34.left = n19
n34.right = n6
display(n12)
stretch(n12, 4)
print("result:")
display(n12)
